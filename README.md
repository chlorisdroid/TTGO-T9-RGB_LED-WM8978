# TTGO-T9-RGB_LED-WM8978

##### THIS IS A FORKED VERSION ######

As the examples provided in the original sources for the most interesting parts as for e.g.
WM8978 are binary only the folder in this repository also contains some simple sourcecode examples for a webradio.


ESP-IDF: https://github.com/espressif/esp-idf

IDF_DEMO: WM8978 (binaries only)

IDF_DEMO: web_radio (sources only)

ARDUINO: https://www.arduino.cc/en/Main/Software

ARDUINO_DEMO: RGB_LED,MPU9250

![Hardware TTGO T9 V1.0](https://gitlab.com/chlorisdroid/TTGO-T9-RGB_LED-WM8978/raw/master/Images/hw_ttgo_t9_v1_0.jpg)

## 1.WM8978
### SD card music player and microphone recording
(1a) Upload the program (original binaries from LilyGo)

    Here we need to use ESP FLASH DOWNLOAD TOOL. If you don't have it installed on your machine download it from the espressif github here:
    
    https://github.com/espressif/esptool and follow their instructions for installing and using it.

    cd into your esp-idf/esptool folder which should be located in

    ~/esp-idf/components/esptool_py/esptool
   
    and upload the binaries from LilyGO with:

    python esptool.py --chip esp32 --port /dev/ttyUSB0 --baud 115200 --before default_reset --after hard_reset write_flash -z --flash_mode dio --flash_freq 80m --flash_size detect 0x1000 /media/tuxi/USB_LIN_EXT4/esp32/TTGO-T9-RGB_LED-WM8978/WM8978/wm8978_code_bin/bootloader.bin 0x10000 /media/tuxi/USB_LIN_EXT4/esp32/TTGO-T9-RGB_LED-WM8978/WM8978/wm8978_code_bin/ESP32_SNOW.bin 0x8000 /media/tuxi/USB_LIN_EXT4/esp32/TTGO-T9-RGB_LED-WM8978/WM8978/wm8978_code_bin/partitions_singleapp.bin

    Make sure that you change the value for your USB Port to your needs: 
       --port /dev/ttyXXX
(1b) compile and upload the the sources for this very basic radio player

     - cd into your local TTGO-T9-RGB_LED-WM8978/web_radio folder
     - set the path environment variables for your esp-idf installation correctly
     - set your wifi credentials: open "Webradio/main/Main.c" in your editor and fill in your SSID and password, save the file and close it.
     - set the default radio station: open "Webradio/main/web_radio.c" and scroll done to line 69.
       change the line from "http_client_get("http://wdr-wdr2-rheinland.icecast.wdr.de/wdr/wdr2/rheinland/mp3/128/stream.mp3", &settings,NULL);" to whatever your favorite station may be.
     - from the commandline enter 
       "make clean" followed by "make defconfig" or "make menuconfig" 
     - compile with "make" or "make -jx" if you have a multicore, where "x" should be number of cores -1
     - flash your esp32 with "make flash"
     - see what's going on with "make monitor". There you should se how the device connects to your router with the internet.
     
     Plug a headset into the connector, lay back and enjoy the day :-)

(2) SD card preparation

    https://github.com/LilyGO/TTGO-T9-RGB_LED-WM8978/tree/master/WM8978/sd_file
    
    First of all to your SD card file backup, and then formatted, it is best not recommended the rest of the paper. 
    Finally, download the file into it.
![image](https://gitlab.com/chlorisdroid/TTGO-T9-RGB_LED-WM8978/raw/master/Images/Screenshot_4.png)

(3) Serial debugging
   
    Insert the SD card,Use putty or minicom and other serial debugging tools shown below
    
![image](https://gitlab.com/chlorisdroid/TTGO-T9-RGB_LED-WM8978/raw/master/Images/Screenshot_7.png)

    At this point we can hear the beautiful music
    
    PS:If you want to modify your songs, you can replace it here 
    
![image](https://gitlab.com/chlorisdroid/TTGO-T9-RGB_LED-WM8978/raw/master/Images/Screenshot_6.png)

(4) Recording function

    Here we need to manually configure WIFI, here I recommend using AI-Think's WeChat public 
    number. Here's the QR code

![image](https://gitlab.com/chlorisdroid/TTGO-T9-RGB_LED-WM8978/raw/master/Images/qrCode.jpg)

<img width="270" height="480" src="https://gitlab.com/chlorisdroid/TTGO-T9-RGB_LED-WM8978/raw/master/Images/photo1.png"/>  <img width="270" height="480" src="https://gitlab.com/chlorisdroid/TTGO-T9-RGB_LED-WM8978/raw/master/Images/photo2.png"/>  <img width="270" height="480" src="https://gitlab.com/chlorisdroid/TTGO-T9-RGB_LED-WM8978/raw/master/Images/photo3.png"/>

![image](https://gitlab.com/chlorisdroid/TTGO-T9-RGB_LED-WM8978/raw/master/Images/Screenshot_1.png)
![image](https://gitlab.com/chlorisdroid/TTGO-T9-RGB_LED-WM8978/raw/master/Images/Screenshot_8.png)

  
